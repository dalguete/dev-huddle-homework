var express = require('express');
var router = express.Router();
var request = require("request");
var rp = require("request-promise");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET homework page. */
router.get('/homework', function(req, res, next) {
  // Get the CN joke
//  rp("http://api.icndb.com/jokes/random")
//    .then(function (body) {
//      var resultsObj1 = JSON.parse(body1);
//      
//    })
//
//  // Get the answer
//  rp("http://yesno.wtf/api/")
//    .then(function(body1) {
//      var resultsObj1 = JSON.parse(body1);
//      res.render('homework', { title: "Chuck is around222!!!", yesno: resultsObj1.image });
//    });


  request.get("http://api.icndb.com/jokes/random/3?escape=javascript", function (err1, res1, body1) {
    if (!err1) {
      var resultsObj1 = JSON.parse(body1);
      var jokes = [];

      resultsObj1.value.forEach(function (element) {
        jokes[jokes.length] = element.joke;
      });

      request.get("http://yesno.wtf/api/", function (err2, res2, body2) {
        if (!err2) {
          var resultsObj2 = JSON.parse(body2);
          var yesno = resultsObj2.image;

          var answer = "";
          if ( resultsObj2.answer === "no" ) {
            answer = "So you don't like them. Two Words: Chuck Norris!. Be careful my friend, be careful.";
          }

          res.render('homework', { title: "Chuck is watching!!!", jokes: jokes, yesno: yesno, answer: answer });
        }
      });
    }
  });
});

module.exports = router;
